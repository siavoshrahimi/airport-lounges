# Airport-lounges
The Airport-Lounges Application built using React Js and ReduxJs. The application uses all the new features introduced in EcmaScript 6. The application is a single page application with proper components and is Mobile Responsive. The Application also used proper authentication which for diffrent role gives different access  and form validation and implements routing uses React Router.
<hr>
<h4>What I used?</h4>
<ul>
<li>React Hooks</li>
<li>React 16 / Jsx pages</li>
<li>React Router</li>
<li>React-Thunk</li>
<li>Redux</li>
<li>Axios</li>
<li>Webpack</li>
<li>Babel</li>
<li>Pure JavaScript</li>
<li>HTML5</li>
<li>CSS3 (Sass)</li>
<hr>
</ul>
<h4>Start by</h4>
<code>npm install</code>
<h5>then</h5>

<code>npm start</code>
