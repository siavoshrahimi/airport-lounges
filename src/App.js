import React,{Fragment ,useEffect} from 'react';
import { connect } from 'react-redux';
import Routs from "./router/routs";
import * as actionCreator from './redux/action';

function App(props) {
  const { onTryAutoSignUp} = props;
  useEffect(() =>{
      onTryAutoSignUp()
  },[onTryAutoSignUp]);
  return (
    <Fragment><Routs/></Fragment>
  );
}
const mapDispatchToProps = dispatch =>{
  return{
    onTryAutoSignUp : () => dispatch(actionCreator.authCheckState())
  }
}
const mapStateToProps = state =>{
  return{
    token: state.auth.token
  }
}
export default connect(mapStateToProps,mapDispatchToProps)( App);
