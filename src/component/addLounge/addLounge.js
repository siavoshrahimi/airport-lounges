import React, {useState} from 'react';
import Input from "../ui/input/inputs";
import Spinner from "../ui/spinner/spinner";
import { connect } from 'react-redux';
import Button from "../ui/button/button";
import * as actionCreator from "../../redux/action";
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";
import axios from '../../assest/api/axios';
import MenuBar from "../menuBar/menuBar";
import './addLounge.scss';

const AddLounge = props =>{
    const [controls , setControls] = useState( {
        airport:{
            elementType:'input',
            elementConfig:{
                type:'input',
                placeholder:'Airport Name'
            },
            valid:false,
            validation:{
                required:true,
            },
            value:'',
            touched:false
        },
        name:{
            elementType:'input',
            elementConfig:{
                type:'input',
                placeholder:'Lounge Name'
            },
            valid:false,
            validation:{
                required:true,
            },
            value:'',
            touched:false
        },
        price:{
            elementType:'input',
            elementConfig:{
                type:'input',
                placeholder:'price'
            },
            valid:false,
            validation:{
                required:true,
            },
            value:'',
            touched:false
        },
        maxCapacity:{
            elementType:'input',
            elementConfig:{
                type:'number',
                placeholder:'Max Capacity'
            },
            valid:false,
            validation:{
                required:true
            },
            value:'',
            touched:false
        },
        imgUrl:{
            elementType:'input',
            elementConfig:{
                type:'input',
                placeholder:'Image URL'
            },
            valid:false,
            validation:{
                required:true,
            },
            value:'',
            touched:false
        },
        available:{
            elementType:'select',
            elementConfig:{
                label:"Available",
                options:[
                    {value:false , displayValue:'No'},
                    {value:true, displayValue:'Yes'}
                ]
            },
            value:'no extra service',
            validation:{},
            valid:true
        },
        information:{
            elementType:'textarea',
            elementConfig:{
                placeholder:'Please introduce Information of Lounge',
            },
            valid:false,
            validation:{
                required:true
            },
            value:'',
            touched:false
        }
    });
    const [formValid , setFormValid] = useState(false);
    const checkValidity = (value, rules) =>{
        let isValid = true;
        if (rules.required){
            isValid = value.trim() !== '' && isValid === true;
        }
        if(rules.minLength){
            isValid = value.length >= rules.minLength && isValid === true;
        }
       
        return isValid
    }
    const inputChangeHandler = (event, inputIdentifier) =>{
        const updatedControls = {...controls};
        const updatedFormElement ={
            ...updatedControls[inputIdentifier]
        }
        updatedFormElement.value = event.target.value;
        updatedFormElement.valid = checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;
        updatedControls[inputIdentifier] = updatedFormElement;
        setControls(updatedControls);
        let formIsValid = true;
        for (let identifier in updatedControls ){
            formIsValid = updatedControls[identifier].valid ===true && formIsValid;
        }
        setFormValid(formIsValid)
    }
    const authHandler = (event) =>{

        event.preventDefault();
        const lounge ={
            "airport":controls.airport.value,
            "name": controls.name.value,
            "price":controls.price.value,
            "info": controls.information.value,
            "max_capacity" : controls.maxCapacity.value,
            "available":controls.available.value,
            "image_url":controls.imgUrl.value,
        }
        props.onAddLounge(props.token, lounge)
    }

    const formElementArray = [];
    for (let key in controls){
        formElementArray.push({
            id:key,
            config:controls[key]
        });
    }
    let form = formElementArray.map(formElement => (
        <Input
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={!formElement.config.valid}
            validation={formElement.config.validation}
            touched={formElement.config.touched}
            changed={(event) =>inputChangeHandler(event,formElement.id)}
        />
    ));
    if(props.loading){
        form = <Spinner/>
    }
    let error = null;
    if(props.error){
        error = (<p>{props.error}</p>)
    }
    return (
        <div className='add-lounge-wrapper'>
            <MenuBar isAuthorized={props.isAdmin} clicked={props.onLogout}/>
            <div className='auth add-lounge'>
                {error}
                <form onSubmit={authHandler}>
                    {form}
                    <Button disabled={!formValid} btnType = "Success">SUBMIT</Button>
                </form>
            </div>
        </div>
    );


}
const mapStateToProps = state =>{
    return{
        loading:state.addLounge.loading,
        error: state.addLounge.error,
        token:state.auth.token ,
        isAdmin:state.auth.isAdmin ,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onAddLounge:(token, lounge) => dispatch(actionCreator.addLounge(token, lounge)),
        onLogout : () => dispatch(actionCreator.logout())
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(withErrorHandler( AddLounge,axios));