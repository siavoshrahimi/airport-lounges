import React ,{ useEffect } from 'react';
import {connect} from 'react-redux';
import axios from '../../assest/api/axios';
import { Link } from 'react-router-dom';
import * as actionCreator from '../../redux/action';
import Spinner from "../ui/spinner/spinner";
import Card from '../card/card';
import MenuBar from "../menuBar/menuBar";
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";
import './home.scss';


const Home = props =>{

    const {token , lounges , error, onLogout , isAuthorized,onInitLounges} = props;
    useEffect(() =>{
        onInitLounges(token)
    },[onInitLounges,token]);

    let lounge = error ?<p>Lounges can't be loaded</p>: <Spinner/>;
    if(lounges){
        lounge = lounges.map(lounge =>(
            <Link
                key={lounge.id}
                to={{
                    pathname:'/detail',
                    state:{
                        id:lounge.id,
                        token:token
                    }
                }}
            >
                <Card data ={lounge}/>
            </Link>
        ))
    }

    return(
        <div className='home-wrapper'>
            <MenuBar isAuthorized ={isAuthorized} clicked={onLogout}/>
            <div className='card-wrapper'>
                {lounge}
            </div>
        </div>
    )
}
const mapDispatchToProps = dispatch =>{
    return{
        onInitLounges: token => dispatch(actionCreator.initLounges(token)),
        onLogout : () => dispatch(actionCreator.logout())
    }
}
const mapStateToProps = state =>{
    return{
        token:state.auth.token,
        lounges: state.lounge.lounges,
        error:state.lounge.error,
        isAuthorized: state.auth.isAdmin
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(withErrorHandler(Home,axios));