import React from 'react';

import './card.scss';

const card = props =>{
    const {image_url,max_capacity,airport,name,price,available,clicked} = props.data;
    return(
        <div className='card' onClick={clicked}>
                <img width="100%" src={image_url? image_url:"https://via.placeholder.com/350"} alt={airport}/>
                <div className='information'>
                    <h3>{airport}</h3>
                    <div>
                        <span>name:{name}</span>
                        <span>price:<strong>{price}</strong></span>
                    </div>
                    <div>
                        <span>capacity:{max_capacity}</span>
                        <span>availability:{available? "Yes" : "No"}</span>
                    </div>
                </div>
        </div>
    )
}


export default card;