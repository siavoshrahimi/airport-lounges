import React from 'react';
import { NavLink } from "react-router-dom";
import Button from "../ui/button/button";
import './menuBar.scss';

const MenuBar = props =>{
    const {isAuthorized,clicked} = props;
    let displayChange;
    isAuthorized ? displayChange ="block" : displayChange = 'none';
    return(
        <div className='menu-bar'>
            <Button clicked={clicked}>Logout</Button>
            <NavLink className='link' to={'/add-lounge'} style={{display: displayChange}}>Add Lounges</NavLink>
            <NavLink className='link' to={'/home'}>Home</NavLink>
        </div>
    )
}
export default MenuBar;