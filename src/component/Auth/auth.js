import React, {useState} from 'react';
import { connect } from 'react-redux';
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";

import * as actionCreator from '../../redux/action';
import history from "../../router/history";
import Input from '../../component/ui/input/inputs';
import Button from '../../component/ui/button/button';
import Spinner from '../../component/ui/spinner/spinner';
import axios from '../../assest/api/axios';
import './auth.scss';
const Auth = props =>{
    const [controls , setControls] = useState( {
        username:{
            elementType:'input',
            elementConfig:{
                type:'input',
                placeholder:'Your Username'
            },
            valid:false,
            validation:{
                required:true,
            },
            value:'',
            touched:false
        },
        password:{
            elementType:'input',
            elementConfig:{
                type:'password',
                placeholder:'password'
            },
            valid:false,
            validation:{
                required:true,
                minLength:6
            },
            value:'',
            touched:false
        }
    });
    const [formValid , setFormValid] = useState(false);

    const checkValidity = (value, rules) =>{
        let isValid = true;
        if (rules.required){
            isValid = value.trim() !== '' && isValid === true;
        }
        if(rules.minLength){
            isValid = value.length >= rules.minLength && isValid === true;
        }
        return isValid
    }
    const inputChangeHandler = (event, inputIdentifier) =>{
        const updatedControls = {...controls};
        const updatedFormElement ={
            ...updatedControls[inputIdentifier]
        }
        updatedFormElement.value = event.target.value;
        updatedFormElement.valid = checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;
        updatedControls[inputIdentifier] = updatedFormElement;
        setControls(updatedControls);

        let formIsValid =true;
        for (let identifier in updatedControls ){
            formIsValid = updatedControls[identifier].valid ===true && formIsValid;
        }
        setFormValid(formIsValid)
    }
    const authHandler = (event) =>{
        event.preventDefault();
        props.onAuth(controls.username.value, controls.password.value);
        props.onAdminCheck(controls.username.value);
    }
    const formElementArray = [];
    for (let key in controls){
        formElementArray.push({
            id:key,
            config:controls[key]
        });
    }
    let form = formElementArray.map(formElement => (
        <Input
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={!formElement.config.valid}
            validation={formElement.config.validation}
            touched={formElement.config.touched}
            changed={(event) =>inputChangeHandler(event,formElement.id)}
        />
    ));
    if(props.loading){
        form = <Spinner/>
    }
    let error = null;
    if(props.error){
        error = (<p>{props.error}</p>)
    }
    let authRedirect = null;
    if(props.isAuthenticated ){
        authRedirect = history.push('/home')
    }
    return (
        <div className='auth'>
            {authRedirect}
            {error}
            <form onSubmit={authHandler}>
                {form}
                <Button disabled={!formValid} btnType = "Success">Sign In</Button>
            </form>
        </div>
    )
}
const mapStateToProps = state =>{
    return{
        loading:state.auth.loading,
        error: state.auth.error,
        isAuthenticated:state.auth.token != null,
        authRedirectPath: state.auth.authRedirectPath,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onAuth:(username, password) => dispatch(actionCreator.auth(username, password)),
        onAdminCheck : username => dispatch(actionCreator.adminCheck(username))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(withErrorHandler(Auth,axios));

