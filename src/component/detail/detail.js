import React , {useEffect,useState} from 'react';
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";
import {connect } from 'react-redux';
import DateTimePicker from 'react-datetime-picker';
import 'react-dates/lib/css/_datepicker.css';
import * as actionCreator from '../../redux/action';
import axios from '../../assest/api/axios';
import Spinner from '../ui/spinner/spinner';
import Input from '../../component/ui/input/inputs';
import Modal from '../ui/modal/modal';
import './detail.scss';
import MenuBar from "../menuBar/menuBar";
import Button from "../ui/button/button";

const Detail = props =>{
    const {token , id} = props.location.state;
    const [loungeData, setLoungeData] = useState(null);
    const [date, setDate] = useState(new Date());
    const [extraService, setExtraService] = useState(
        {extra:{
            elementType:'select',
            elementConfig:{
                options:[
                    {value:'no extra service' , displayValue:'no extra service'},
                    {value:'meeting/team business room from 1 to 4 people' , displayValue:'meeting/team business room from 1 to 4 people'},
                    {value:'access to functional fitness centre with showers' , displayValue:'access to functional fitness centre with showers'},
                    {value:'access to spa/massage rooms' , displayValue:'access to spa/massage rooms'},
                ]
            },
            value:'no extra service',
            validation:{},
            valid:true
        }});
    useEffect(() => {
        let config = {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        }
        axios.get(`/api/v1/lounges/${id}`,config)
            .then(res => setLoungeData( res.data))
            .catch(err =>err);
    },[id,token]);

    const inputChangeHandler = (event) =>{
        const updatedControls = {...extraService};
        updatedControls.extra.value = event.target.value;
        updatedControls.extra.touched = true;
        setExtraService(updatedControls)
    }

    const bookHandler = event =>{
        event.preventDefault();
        props.onBooking(id,date.toDateString(),date.toTimeString(),extraService.extra.value,token);
    }

    const dateHandler = (date) =>{
        setDate(date);
    }
    const closeModalHandler = () => {
        props.onCloseSuccessMassage();
    }
    const deleteHandler= (token,id) =>{
        props.onDeleteLounge(token,id);
    }
    let detail = <Spinner/>;
    if(loungeData){
        detail =
            <div className='detail'>
                <div className='detail-description'>
                    <Modal
                        show={props.isBooking}
                        modalClosed={closeModalHandler}
                    >
                        {props.congText? props.congText:null}
                    </Modal>
                    <img src={loungeData.image_url} alt={loungeData.airport}/>
                    <h3>Airport:{loungeData.airport}</h3>
                    <div className='detail-info'>
                        <span>Name:{loungeData.name}</span>
                        <span>Price:<strong>{loungeData.price}</strong></span>
                        <span>Capacity:{loungeData.max_capacity}</span>
                        <span>Availability:{loungeData.available? 'Yes':'No'}</span>
                    </div>
                    <h4>description:</h4>
                    <p>{loungeData.info}</p>
                </div>
                <div className='booking'>
                    <div className='date-picker-wrapper'>
                        <h2>Book it Now!</h2>
                        <DateTimePicker
                            onChange={dateHandler}
                            value={date}
                            minDate={new Date()}
                            className='date-picker'
                        >
                        </DateTimePicker >
                    </div>
                        <Input
                            elementType={extraService.extra.elementType}
                            elementConfig={extraService.extra.elementConfig}
                            value={extraService.extra.value}
                            invalid={!extraService.extra.valid}
                            validation={extraService.extra.validation}
                            touched={extraService.extra.touched}
                            changed={(event) =>inputChangeHandler(event)}
                        >
                            Extra Services
                        </Input>
                        <Button btnType = "Success" clicked={bookHandler}>BOOK</Button>
                        <Button
                            btnType="Danger"
                            disabled={!props.isAdmin}
                            clicked={() => deleteHandler(token,id)}
                        >Delete
                        </Button>


                </div>


            </div>
    }

    return(
        <div className='detail-wrapper'>
            <MenuBar isAuthorized={props.isAdmin} clicked={props.onLogout}/>
            {detail}
        </div>
    )
}
const mapStateToProps = state =>{
    return{
        congText: state.book.congText,
        isBooking:state.book.isBooking,
        isAdmin: state.auth.isAdmin
    }
}
const mapDispatchToProps = dispatch =>{
    return {
        onBooking : (id,date,time,extra,token) => dispatch(actionCreator.book(id,date,time,extra,token)),
        onCloseSuccessMassage : () => dispatch(actionCreator.closeSuccessMassage()),
        onDeleteLounge : (token,id) =>dispatch(actionCreator.deleteLounge(token,id)),
        onLogout : () => dispatch(actionCreator.logout())
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(withErrorHandler(Detail,axios));