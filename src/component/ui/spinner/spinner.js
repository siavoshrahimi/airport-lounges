import React from 'react';

import './spinner.scss';
const spinner = () => (
    <div className="Loader">Loading...</div>
)

export default spinner;