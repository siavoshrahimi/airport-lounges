import React,{Suspense,lazy} from 'react';
import {Route ,Router,Switch,Redirect} from 'react-router-dom';
import { connect } from 'react-redux';
import history from "./history";

import Auth from '../component/Auth/auth';

const Home = lazy(()=> import('../component/landingPage/home'));
const Detail = lazy(()=> import('../component/detail/detail'));
const AddLounge = lazy(()=> import('../component/addLounge/addLounge'));


const Routs = props =>{
    let routes = (
        <Switch>
            <Route path='/auth' component={Auth} exact/>
            <Redirect to='/auth' />
        </Switch>
    )

    if(props.isAuthenticated) {
        routes =(
            <Switch>
                <Route path='/detail' component={Detail}/>
                <Route path='/home' component={Home}/>
                <Route path='/auth' component={Auth} exact/>
                <Redirect to='/home' />
            </Switch>
        )
    }
    if(props.isAuthenticated && props.isAdmin){
        routes =(
            <Switch>
                <Route path='/detail' component={Detail}/>
                <Route path='/home' component={Home}/>
                <Route path='/add-lounge' component={AddLounge}/>
                <Route path='/auth' component={Auth} exact/>
                <Redirect to='/home' />
            </Switch>
        )
    }
    return(
        <Router history={history}>
            <Suspense fallback={null}>
                {routes}
            </Suspense>
        </Router>
    )
}

const mapStateToProps = state =>{
    return {
        isAuthenticated: state.auth.token,
        isAdmin: state.auth.isAdmin
    }
}

export default connect(mapStateToProps)(Routs);