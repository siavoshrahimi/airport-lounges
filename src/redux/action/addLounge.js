import * as actionType from './actionType';
import axios from '../../assest/api/axios';
import histor from "../../router/history";

export const addLoungeStart = () =>{
    return{
        type:actionType.ADD_LOUNGE_STARTED,
    }
}

export const addLoungeSuccess = () =>{
    return{
        type:actionType.ADD_LOUNGE_SUCCESS
    }
}

export const addLoungeFailed = err =>{
    return{
        type:actionType.ADD_LOUNGE_FAILED
    }
}

export const addLounge = (token, lounge) =>{
    return dispatch =>{
        let config = {
            headers : {
                'Authorization': 'Bearer ' + token,
                'content-type':'application/json',
            }
        }
        dispatch(addLoungeStart());
        axios.post('/api/v1/admin/lounges',lounge,config)
            .then(res => {
                dispatch(addLoungeSuccess());
                histor.replace('/home');
             })
            .catch(err => dispatch(addLoungeFailed(err)));
    }
}