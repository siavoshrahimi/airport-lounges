import * as actionType from './actionType';
import axios from '../../assest/api/axios';
export const setLounges = lounges =>{
    return{
        type:actionType.SET_LOUNGES,
        lounges:lounges
    }
}

export const fetchLoungesFailed = err =>{
    return{
        type:actionType.FETCH_LOUNGES_FAILED,
        error:err
    }
}

export const initLounges = (token) =>{
    let config = {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    }
    return dispatch =>{
        axios.get('/api/v1/lounges',config)
            .then(res =>dispatch(setLounges(res.data.lounges)))
            .catch(err =>dispatch(fetchLoungesFailed(err)))
    }
}