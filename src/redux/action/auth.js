import * as actionType from './actionType';
import axios from '../../assest/api/axios';
import history from "../../router/history";

export const authStart = () =>{
    return{
        type:actionType.AUTH_START
    }
}

export const authSuccessAdmin = token => {
    return{
        type:actionType.AUTH_SUCCESS_ADMIN,
        token:token
    }
}
export const authSuccessUser = token => {
    return{
        type:actionType.AUTH_SUCCESS_USER,
        token:token
    }
}
export const authFailed = err =>{
    return{
        type:actionType.AUTH_FAILED,
        error:err
    }
}

export const auth = (user,password) =>{
    return dispatch => {
        dispatch(authStart());
        const authData = {
            "username":user,
            "password":password
        }
        const headers= {
            contentType:'application/json',
        }
        axios.post('/login',authData,headers)
            .then(res => {
                const userIdentiFire = {token:res.data.token , role:user};
                localStorage.setItem('user',JSON.stringify(userIdentiFire));
                if(user ==="admin"){
                    dispatch(authSuccessAdmin(res.data.token))
                }else {
                    dispatch(authSuccessUser(res.data.token))
                }

            })
            .catch(err => {
                dispatch(authFailed(err.response.data.error));
            });
    }
}
export const logout = () =>{
    localStorage.removeItem('user');
    //dispatch(setAuthRedirectPath('/'));
    history.replace("/auth");
    return{
        type:actionType.LOGOUT
    }
}
export const authCheckState = () => {
    return dispatch => {
        const user = JSON.parse(localStorage.getItem('user'));
        if (user === null) {
            dispatch(logout());
        } else {
            if (user.token && user.role === "admin") {
                dispatch(authSuccessAdmin(user.token));
            } else {
                dispatch(authSuccessUser(user.token))
            }

        }
    };
};

export const adminCheck = (username) => {
    return{
        type:actionType.ADMIN_CHECK,
        username: username === 'admin'
    }
}
