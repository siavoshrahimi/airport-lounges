import * as actionType from './actionType';
import axios from '../../assest/api/axios';
import history from "../../router/history";


export const closeSuccessMassage = () =>{
    return{
        type:actionType.CLOSE_SUCCESS_MASSAGE
    }
}
export const bookStart = () => {
    return{
        type:actionType.BOOK_START,
    }
}

export const bookSuccess = (congText) =>{
    return{
        type:actionType.BOOK_SUCCESS,
        congText:congText
    }
}

export const bookFailed = (err) =>{
    return{
        type:actionType.BOOK_FAILED,
        error:err
    }
}

export const book = (id,date,time,extra,token) =>{
    return dispatch =>{
        dispatch(bookStart());
        const bookData = {
            "booking_date":date,
            "booking_hour":time,
            "extra_services":{"extra":extra}
        }
        let config = {
            headers : {
                'Authorization': 'Bearer ' + token,
                'content-type':'application/json',
            }
        }

        axios.post(`/api/v1/lounges/${id}/book`,bookData,config)
            .then(res => {
                dispatch(bookSuccess("Congratulation You Booked Correctly"));
            })
            .catch(err => {
                dispatch(bookFailed(err))
            })

    }
}
export const deleteStart = () =>{
    return{
        type:actionType.DELETE_START
    }
}
export const deleteSuccess = () =>{
    return{
        type:actionType.DELETE_SUCCESS
    }
}
export const deleteFailed = err =>{
    return{
        type:actionType.DELETE_FAILED,
        error:err
    }
}
export const deleteLounge = (token,id) =>{
    return dispatch =>{
        let config = {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        }
        dispatch(deleteStart());
        axios.delete(`/api/v1/admin/lounges/${id}`,config)
            .then(res => {
                dispatch(deleteSuccess());
                history.replace("/home");
            })
            .catch(err => deleteFailed(err))
        }
    }

