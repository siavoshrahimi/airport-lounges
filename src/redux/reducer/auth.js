import * as actionType from '../action/actionType';

const initialState = {
    token: null,
    loading:false,
    error:null,
    isAdmin:false,
    congText:null
}

const reducer = (state= initialState, action) =>{
    switch (action.type) {
        case actionType.AUTH_START:
            return{
                ...state,
                loading: true,
                error: null
            }
        case actionType.AUTH_SUCCESS_ADMIN:
            return {
                ...state,
                token:action.token,
                error: null,
                loading: false,
                isAdmin: true
            }
        case actionType.AUTH_SUCCESS_USER:
            return {
                ...state,
                token:action.token,
                error: null,
                loading: false,
                isAdmin: false
            }
        case actionType.AUTH_FAILED:
            return {
                ...state,
                error: action.error,
            }
        case actionType.LOGOUT:
            return {
                ...state,
                token: null,
                error: null,
                loading: false
            }
        case actionType.ADMIN_CHECK:
            return {
                ...state,
                isAdmin: action.username
            }
        case actionType.BOOK_FAILED:
            return {
                ...state,
                error: action.error,
                loading: false
            }
        case actionType.BOOK_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
                congText : action.congText
            }
        case actionType.BOOK_START:
            return {
                ...state,
                loading: true,
                error: null
            }
        default:
            return state

    }
}
export default reducer;