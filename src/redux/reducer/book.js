import * as actionType from '../action/actionType';

const initialState = {
    loading:false,
    error:null,
    congText:null,
    isBooking:false
}

const reducer = (state= initialState, action) =>{
    switch (action.type) {
        case actionType.BOOK_FAILED:
            return {
                ...state,
                error: action.error,
                loading: false
            }
        case actionType.BOOK_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
                congText : action.congText,
                isBooking: true
            }
        case actionType.BOOK_START:
            return {
                ...state,
                loading: true,
                error: null
            }
        case actionType.CLOSE_SUCCESS_MASSAGE:
            return {
                ...state,
                congText: null,
                isBooking:false
            }
        case actionType.DELETE_START:
            return {
                ...state,
                error: null,
                loading: true
            }
        case actionType.DELETE_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
            }
        case actionType.DELETE_FAILED:
            return {
                ...state,
                error: action.error,
                loading: false
            }
        default:
            return state

    }
}
export default reducer;