import * as actionType from '../action/actionType';

const initialState = {
    error:null,
    loading:false,

}

const reducer = (state=initialState,action) =>{
    switch (action.type) {
        case actionType.ADD_LOUNGE_STARTED:
            return{
                ...state,
                error:null,
                loading: true
            }
        case actionType.ADD_LOUNGE_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false
            }
        case actionType.ADD_LOUNGE_FAILED:
            return {
                ...state,
                error: action.error,
                loading: false
            }
        default:
            return state
    }
}
export default reducer;
