import * as actionType from '../action/actionType';

const initialState = {
    lounges:null,
    error:null
}

const reducer = (state = initialState, action) =>{
    switch (action.type) {
        case actionType.SET_LOUNGES:
            return{
                ...state,
                lounges: action.lounges,
                error: null,
            }
        case actionType.FETCH_LOUNGES_FAILED:
            return {
                ...state,
                error: action.error
            }
        default:
            return state
    }
}

export default reducer;