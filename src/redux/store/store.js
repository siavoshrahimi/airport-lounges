import {createStore,combineReducers, compose, applyMiddleware} from 'redux';
import ReduxThunk from 'redux-thunk';

import authReducer from '../reducer/auth';
import loungeReducer from '../reducer/lounge';
import bookReducer from '../reducer/book';
import addLoungeReducer from '../reducer/addLounge';

const rootReducer = combineReducers({
    auth: authReducer,
    lounge:loungeReducer,
    book:bookReducer,
    addLounge:addLoungeReducer
})

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default ()=>{
    const store = createStore(rootReducer,
        composeEnhancer(applyMiddleware(ReduxThunk))
    );
    return store
}
